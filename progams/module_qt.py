"""
idée a faire :
cinématique de premier lancement,
    - nom de la personne
    - debrief des commandes possibles
    - le "soyez indulgent"
    - page d'explication des boutons etc
    - pas de soucis si vous voulez un maj/ review debug
    - auteur
"""

from PySide6 import QtCore, QtWidgets,QtGui
from PySide6 import QtWidgets as wid
import sys
import json                 as js
import speech_recognition   as sr
import pyttsx3              as tts

import random               as rd
import webbrowser           as web
import datetime
import os

class FenQT(QtWidgets.QWidget):

    def __init__(self) -> None :
        super().__init__()

        # création de la liste des input disponible
        self.setDictMic()
        self.GetSave()

        self.main = wid.QHBoxLayout()
        self.struct = {
            "prinp" : wid.QHBoxLayout(),
            "entre" : {
                "box"    : wid.QGroupBox("Parametre d'entré"),
                "layout" : wid.QFormLayout(),
                "1.1"    : wid.QLabel("Micros :"),
                "1.2"    : wid.QComboBox(), # 
                "2.1"    : wid.QLabel("Mot d'appel"), 
                "2.2"    : wid.QLineEdit(), # barre d'entrer 
                "3.1"    : wid.QPushButton("help"), # bouton help
                "3.2"    : wid.QPushButton("ON/OFF"), # bouton Save
                "4.1"    : wid.QLabel(""), # bouton on off
                "4.2"    : wid.QLabel('Etat du bot : OFF'),  # voyant pour voir si le bot est actif # Error
            },
            "voix" : {
                "box"      : wid.QGroupBox("Paramètre de la Voix"),
                "layout"   : wid.QFormLayout(),
                "layV" : wid.QHBoxLayout(),
                "slideVol" : wid.QFormLayout(),
                "slideVit" : wid.QFormLayout(),
                "1.5.1"    : wid.QPushButton("Test de la Voix"),

                "2.1.1"    : wid.QLabel("Volume"), # nom du slider
                "2.2.1"    : wid.QLabel("  +"),
                "2.3.1"    : wid.QSlider(), # slider en lui meme
                "2.4.1"    : wid.QLabel("  -"),
                
                "2.1.2"    : wid.QLabel("Vitesse"), # nom du slider
                "2.2.2"    : wid.QLabel("  +"),
                "2.3.2"    : wid.QSlider(), # slider en lui meme
                "2.4.2"    : wid.QLabel("  -"),
            },
            "settings" : {
                "box"      : wid.QGroupBox("Paramètre de l'application"),
                "layout"   : wid.QFormLayout(),
                "1.1"      : wid.QCheckBox("Mise en Arrière plan"),
                "1.2"      : wid.QCheckBox("Auto-startup"),
                "1.3"      : wid.QCheckBox("Retenir la position"),
                "1.4"      : wid.QPushButton("Info"),
                "1.5"      : wid.QPushButton("Save"),
            }
        }
        self.setwindow()

    def GetSave(self):
        with open('progams\settings.json','r') as mon_fichier :
            self.setting = js.load(mon_fichier)

    def setwindow(self) :
        # Parti parametres des entrees
        self.struct["entre"]["layout"].addRow(self.struct["entre"]["1.1"],self.struct["entre"]["1.2"]) # micro : , combobox
        self.struct["entre"]["layout"].addRow(self.struct["entre"]["2.1"],self.struct["entre"]["2.2"]) # mot d'appel , LineEdit
        self.struct["entre"]["layout"].addRow(self.struct["entre"]["3.1"],self.struct["entre"]["3.2"]) # help , on/off
        self.struct["entre"]["layout"].addRow(self.struct["entre"]["4.1"],self.struct["entre"]["4.2"]) # on/off, etat du bot
        ## création de la partie des parametre des entrées
        self.struct["entre"]["box"]   .setLayout(self.struct["entre"]["layout"])
        self.struct["prinp"]          .addWidget(self.struct["entre"]["box"])# ajout de la partie à la fenetre principale

        # Parti parametres de la voix 
        ### mise en place de la partie de Volume
        self.struct["voix"]["slideVol"].addRow(self.struct["voix"]["2.1.1"]) # volume 
        self.struct["voix"]["slideVol"].addRow(self.struct["voix"]["2.2.1"]) # +
        self.struct["voix"]["slideVol"].addRow(self.struct["voix"]["2.3.1"]) # slidevol
        self.struct["voix"]["2.3.1"].setRange(75, 120)
        self.struct["voix"]["slideVol"].addRow(self.struct["voix"]["2.4.1"]) # -
        ### mise en place de la partie de Vitesse
        self.struct["voix"]["slideVit"].addRow(self.struct["voix"]["2.1.2"])
        self.struct["voix"]["slideVit"].addRow(self.struct["voix"]["2.2.2"])
        self.struct["voix"]["slideVit"].addRow(self.struct["voix"]["2.3.2"])
        self.struct["voix"]["2.3.2"].setRange(100, 250)
        self.struct["voix"]["slideVit"].addRow(self.struct["voix"]["2.4.2"])
        ### conself.struction de la partie de gauche
        self.struct["voix"]["layV"].addLayout(self.struct["voix"]["slideVol"])
        self.struct["voix"]["layV"].addLayout(self.struct["voix"]["slideVit"])
        ### conself.struction de la partie parametre de la voix
        self.struct["voix"]["layout"] .addRow(self.struct["voix"]["1.5.1"])
        self.struct["voix"]["layout"] .addRow(self.struct["voix"]["layV"])
        self.struct["voix"]["box"]    .setLayout(self.struct["voix"]["layout"])
        self.struct["prinp"]          .addWidget(self.struct["voix"]["box"]) # ajout de la partie à la fenetre principale

        # Parti des settings de l'application
        self.struct["settings"]["layout"].addRow(self.struct["settings"]["1.1"]) # mise en arriere plan
        self.struct["settings"]["layout"].addRow(self.struct["settings"]["1.2"]) # auto startup
        self.struct["settings"]["layout"].addRow(self.struct["settings"]["1.3"]) # positionnement
        self.struct["settings"]["layout"].addRow(self.struct["settings"]["1.4"]) # info
        self.struct["settings"]["layout"].addRow(self.struct["settings"]["1.5"]) # save
        self.struct["settings"]["box"]   .setLayout(self.struct["settings"]["layout"])
        self.struct["prinp"]             .addWidget(self.struct["settings"]["box"]) # ajout de la partie à la fenetre principale

        self.setbefore()
        self.setAction()
        self.main.addLayout(self.struct["prinp"])
        self.setLayout(self.main)
        self.setWindowTitle("Guppy")

    def setbefore(self) : 
        self.struct["entre"]["2.2"].insert(self.setting["AssName"])

        self.struct["voix"]["2.3.1"].setValue(self.setting['Volume'])
        self.struct["voix"]["2.3.2"].setValue(self.setting['Vitesse'])

        if self.setting["isBackGround"] == True : 
            self.struct["settings"]["1.1"].setChecked(True)
        else : 
            self.struct["settings"]["1.1"].setChecked(False)
        if self.setting["isAutoStartup"] == True : 
            self.struct["settings"]["1.2"].setChecked(True)
        else : 
            self.struct["settings"]["1.2"].setChecked(False)
        if self.setting["isPositioned"] == True : 
            self.struct["settings"]["1.3"].setChecked(True)
        else : 
            self.struct["settings"]["1.3"].setChecked(False)

    def setAction(self) :

        self.struct["entre"]["1.2"].addItems(self.dictDevices.values()) # don de la liste des micros au combobox
        if self.setting["NameMic"] not in self.dictDevices.values() : # set de la valeur a afficher au lancement
            self.struct["entre"]["1.2"].setCurrentText(self.dictDevices[404]) # valeur si le micro n'est pas dispo
        else : # s'il y est : on détermine son key dans le dico (key = index de la liste donner par sr)
            for key, value in self.dictDevices.items() : 
                if self.setting["NameMic"] == value :
                    self.struct["entre"]["1.2"].setCurrentText(self.dictDevices[key])
                    break
        self.struct["entre"]["1.2"].currentTextChanged.connect(self.ActCombobox) # fonction à exe si changement
        self.struct["entre"]["2.2"].textEdited.connect(self.ActLineEdit) # fonction à exe si changement
        self.struct["entre"]["3.1"].clicked.connect(self.ActHelp) # fonction à exe si changement

        self.struct["voix"]["2.3.1"].sliderMoved.connect(self.ActVolSlide)
        self.struct["voix"]["2.3.2"].sliderMoved.connect(self.ActVitSlide)
        self.struct["voix"]["1.5.1"].clicked.connect(self.ActTest)

        self.struct["settings"]["1.1"].stateChanged.connect(self.ActBackground)
        self.struct["settings"]["1.2"].stateChanged.connect(self.ActAutoStartup)
        self.struct["settings"]["1.3"].stateChanged.connect(self.ActPositioned)
        self.struct["settings"]["1.4"].clicked.connect(self.ActInfo)
        self.struct["settings"]["1.5"].clicked.connect(self.ActSave)

    def setDictMic      (self) :        # refresh du dictionnaire des Micros
        self.dictDevices = {404:"No mic found"}
        for device in sr.Microphone.list_microphone_names() : 
            if device not in self.dictDevices :
                self.dictDevices[sr.Microphone.list_microphone_names().index(device)] = device
    def ActCombobox     (self,value) :  # setAction du combobox
        """présave du nom du micro"""
        self.setting['NameMic'] = value
    def ActLineEdit     (self,value) :  # setAction du nom d'appel
        """présave du nom d'appel"""
        self.setting['AssName'] = value
    def ActHelp         (self) :        # setAction du bouton Help
        """envoie vers le lien de la page gitlab (doit changer)"""
        web.open('https://gitlab.com/lezib/pc_assistant')
    def ActVolSlide     (self,value) :  # setAction du slider du volume de la voix
        """présave du volume de la voix"""
        self.setting["Volume"] = value
    def ActVitSlide     (self,value) :  # setAction du slider de la vitesse de la voix
        """présave de la vitesse de la voix"""
        self.setting["Vitesse"] = value
    def ActBackground   (self) :        # setAction de la checkbox du parametre background
        """inversion du parametre"""
        self.setting["isBackGround"] = not self.setting["isBackGround"]
    def ActAutoStartup  (self) :        # setAction de la checkbox du parametre Auto-Start
        """inversion du parametre"""
        self.setting["isAutoStartup"] = not self.setting["isAutoStartup"]
    def ActPositioned   (self) :        # setAction de la checkbox du parametre position
        """inversion du parametre"""
        self.setting["isPositioned"] = not self.setting["isPositioned"]
    def ActInfo         (self) :        # setAction du bouton informations
        """ouvre la page gitlab (doit etre changé)"""
        web.open("https://gitlab.com/lezib/pc_assistant")
    def ActSave         (self) :        # setAction du bouton Save
        def ActWinSave() : # fonction fermant la fenetre d'information 
            msg.done(1)
            self.startBot() # reload du bot avec les changements
        with open('progams\settings.json','w') as mon_fichier : # ouverture du fichier en mode ecriture
            js.dump(self.setting, mon_fichier)
        
        msg = wid.QMessageBox() # definition du widget
        msg.setIcon(wid.QMessageBox.Information) # set de l'icone du widget (interieur)
        msg.setText("Statut :") # titre de l'information 
        msg.setInformativeText("C'est bon tout est sauvegardé.") # précisions de l'information
        msg.setWindowTitle("Guppy") # titre de la fenetre d'information
        msg.setStandardButtons(wid.QMessageBox.Ok) # definition du bouton de sortie
        msg.buttonClicked.connect(ActWinSave) # setAction du bouton "OK" (=sortie de cette fenetre)
        msg.exec() # execution de la fenetre une fois qu'elle est prête
    def ActTest         (self) :        # setAction du bouton Test
        """Fait dire un test au bot"""
        listtest = ['Ceci est un très beau test','un jour je serai le meilleur dresseur','Je voue un culte a mon chat tout les soirs','Saviez vous ? le briquet a été inventé avant les allumettes','il faut utiliser le gestionnaire de mots de passe kipass',"Les pâtes en papillons c'est un peu surcoté","Comment les oiseaux peuvent manger des chewing gums s'ils ont pas de dents?"] # dictionnaire des phrase de test
        test = listtest[rd.randint(0,len(listtest))] # selection au hasard d'une phrase de test
        self.speak(test) # pronociation de la phrase de test

    ################################################################
    # commencement de la definition du bot de diction 
    def startBot        (self) :        # set du bot et lancement de ce dernier
        self.voice = tts.init('sapi5') # initialisation du bot
        self.voice.setProperty('voice','french') # set de la langue
        self.voice.setProperty('rate', self.setting["Vitesse"]) #  set de la vitesse de la vitesse de la voix
        self.voice.setProperty('volume', self.setting["Volume"]*0.01) # set du volume de la voix
        
        self.traducteur = sr.Recognizer() # initialisation du traducteur
        self.hear.pause_threshold = 1 # il faut une seconde pour que la phrase soit considérée comme finie

    def speak(self,text:str) :          # fonction de diction 
        self.voice.say(text) # lancement de la diction
        self.voice.runAndWait() # attente que la phrase soit finie
    def listen(self) :

        with sr.Microphone(device_index=self.dictDevices[self.settings]) as source:
            print("--------------------------------")
            self.traducteur.pause_threshold = 1
            audio = self.traducteur.listen(source)
    
        try:
            self.query = self.traducteur.recognize_google(audio, language ='fr-FR')
            print(f"> {self.query}")
            return self.query.lower()
    
        except Exception as e:
            print(e)   
            print("> failed") 
            return None

    def Main(self) : 
        self.startBot()
        self.run = True
        while self.run :
            self.DoCommand(self.listen())

    def DoCommand(self,task:str) :
        month = {'1':'janvier', '2':'février','3':'Mars','4':'Avril','5':'Mai','6':'Juin','7':'Juillet','8':'Août','9':'septembre','10':'octobre','11':'novembre','12':'decembre'}
        day = {1:'Lundi', 2:'Mardi',3:'Mercredi',4:'Jeudi', 5:'Vendredi',6:'Samedi',7:'Dimanche'}
        # traitement pré-analyse
        if task is None : # dans le cas ou le micro n'a rien reconnu (return)
            return
        if self.settings["AssName"] not in task : # si le mot d'appel ne figure pas dans le texte reconnu (return)
            return 
        else : # si les deux condition son remplies alors on continue et on isole la request
            task = task[task.find(self.settings["AssName"])+len(self.settings["AssName"])+1:]

        # information vocale
        if "comment va-tu" or 'ça va' or "comment tu va" in task :
            self.speak('bien merci !')
            return
        elif "quelle heure est-il" or "il est quelle heure" or "donne moi l'heure" in task :
            self.speak('il est',datetime.datetime.now().strftime("%H heures et %M minutes"))
            return
        elif "on est quelle jour" or "quelle jour est t-il" or "donne moi la date" in task :
            mois = month[datetime.date.today().strftime("%m")]
            self.speak('on est le',datetime.date.today().strftime("%d"),mois)
        elif "on est quelle jour de la semaine" in task :
            self.speak('on est le',day[datetime.date.today().weekday()],datetime.date.today().strftime("%d"))

        # execution web
        elif "ouvre" in task :
            for Site, link in self.settings["SitePredefined"].items():
                if Site in task :
                    try : self.speak("ouverture de",Site)
                    except : 
                        self.speak("Vous n'avez pas de raccourci pour",Site)
                        return
                    try : web.open(link)
                    except :
                        self.speak("erreur, fichier introuvable, veuillez vérifier le chemin d'accès dans le menu prévu pour")
                        return
                    return
        # execution de programme
        elif "démarre" in task : 
            for App, Path in self.settings["AppPath"].items():
                if App in task :
                    try : self.speak("démarrage de",App)
                    except : 
                        self.speak("Vous n'avez pas de raccourci pour",App)
                        return
                    try : os.startfile(Path)
                    except :
                        self.speak("erreur, fichier introuvable, veuillez vérifier le chemin d'accès dans le menu prévu pour")
                        return

if __name__ == "__main__":
    app = QtWidgets.QApplication([])

    fen = FenQT() # creation de la fenetre 
    fen.show() # apparition de la fenetre et garde de vie de la fenetre
    #sys.exit()
    sys.exit(app.exec()) # quit du programme propement  