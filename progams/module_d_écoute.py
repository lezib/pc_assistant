import speech_recognition as sr
import pyttsx3 as ttx
import sys

class bot :
    def __init__(self) -> None:
        self.oreille = sr.Recognizer() # création de l'oreille afin de transformer un audio en texte
        self.microchannel = sr.Microphone(device_index=2) # set du micro choisi
        self.engine = ttx.init() # initialisation de la voix robotisée
        self.voix = self.engine.getProperty('voices') # recup des informations sur la voix pour les appliquer
        self.engine.setProperty('voice','french')
        # self.ecoute()
        self.counter = 0
        while True :
            self.takeCommand()
        pass

    def ecoute(self) : 
        with self.microchannel as source:
            print("go")
            audio_data = self.oreille.listen(source)
            print("end")
        result = self.oreille.recognize_google(audio_data,language='fr-FR')
        result.lower()
        print(result)

        #pour ne pas surchargé de demande : tts.isbusy

    def takeCommand(self):
     
        r = sr.Recognizer()
        
        with sr.Microphone() as source:
            
            print("Listening...")
            r.pause_threshold = 0.60
            audio = r.listen(source)
    
        try:
            print("Recognizing...")   
            query = r.recognize_google(audio, language ='fr-FR')
            print(f"User said: {query}")
            if query == "stop" :
                sys.exit()
            self.engine.say(query)
            self.engine.runAndWait()

    
        except Exception as e:
            print(e)   
            print("Unable to Recognize your voice.") 
        self.counter += 1

        print(self.counter)




bot()  